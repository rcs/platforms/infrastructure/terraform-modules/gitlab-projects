variable "name" {
  default = ""
  type    = string
}
variable "archived" {
  default = false
  type    = bool
}

variable "path" {
  default = ""
  type    = string
}

variable "parent_id" {
  default     = 0
  type        = number
  description = "ID of the parent group"
}

variable "description" {
  default = ""
  type    = string
}

variable "visibility" {
  default = "private"
  type    = string
}

variable "default_branch" {
  default = "master"
  type    = string
}

variable "approvals_before_merge" {
  default = 0
}

variable "wiki_enabled" {
  default = true
  type    = bool
}

variable "snippets_enabled" {
  default = true
  type    = bool
}

variable "only_allow_merge_if_pipeline_succeeds" {
  default = false
  type    = bool
}

variable "only_allow_merge_if_all_discussions_are_resolved" {
  default = false
  type    = bool
}

variable "container_registry_enabled" {
  default = true
  type    = bool
}

variable "issues_enabled" {
  default = true
  type    = bool
}


variable "shared_runners_enabled" {
  default = false
  type    = bool
}


variable "tags" {
  type    = list
  default = []
}

variable "webhooks" {
  type    = list
  default = []
}

variable "enable_branch_protection" {
  type    = list
  default = []
}

variable "jira_url" {
  default = ""
  type    = string
}


variable "jira_project" {
  default = ""
  type    = string
}

variable "jira_issue_transition_id" {
  default = "2"
  type    = number
}

variable "jira_username" {
  default = ""
  type    = string
}

variable "jira_password" {
  default = ""
  type    = string
}

variable "slack_webhook_url" {
  default = ""
  type    = string
}

variable "slack_username" {
  default = ""
  type    = string
}


variable "slack_push_events" {
  default = true
  type    = bool
}

variable "slack_push_channel" {
  default = ""
  type    = string
}

variable "slack_notify_only_broken_pipelines" {
  default = false
  type    = string
}

variable "slack_notify_only_default_branch" {
  default = false
  type    = bool
}

variable "slack_issues_events" {
  default = false
  type    = bool
}

variable "slack_issue_channel" {
  default = ""
  type    = string
}

variable "slack_confidential_issues_events" {
  default = false
  type    = bool
}

variable "slack_confidential_issue_channel" {
  default = ""
  type    = string
}

variable "slack_tag_push_events" {
  default = false
  type    = bool
}

variable "slack_tag_push_channel" {
  default = ""
  type    = string
}

variable "slack_note_events" {
  default = false
  type    = bool
}

variable "slack_note_channel" {
  default = ""
  type    = string
}

variable "slack_confidential_note_events" {
  default = false
  type    = bool
}

variable "slack_pipeline_events" {
  default = false
  type    = bool
}

variable "slack_pipeline_channel" {
  default = ""
  type    = string
}

variable "slack_wiki_page_events" {
  default = false
  type    = bool
}

variable "slack_wiki_page_channel" {
  default = ""
  type    = string
}

#TagProtect Variables

variable "tag_to_protect" {
  default     = "*"
  type        = string
  description = "Tag to protect. Defaults to wildcard"
}

variable "tag_create_access_level" {
  default     = "developer"
  type        = string
  description = "Access level required to update tag. Defaults to Developer"
}

# Push Rule Variables
variable "push_rule_commit_message_regex" {
  default     = ""
  type        = string
  description = "Optional: commit message control via regex"
}

variable "push_rule_prevent_secrets" {
  default     = false
  type        = bool
  description = "Optional: If true Gitlab will try to stop secrets being pushed"
}

variable "push_rule_branch_name_regex" {
  default     = ""
  type        = string
  description = "Optional: Branch name control via regex"
}

variable "push_rule_author_email_regex" {
  default     = ""
  type        = string
  description = "Optional: author control via regex"
}

variable "push_rule_commit_committer_check" {
  default     = true
  type        = bool
  description = "Optional: Users can only push commits to this repository that were committed with one of their own verified emails"
}

variable "push_rule_file_name_regex" {
  default     = ""
  type        = string
  description = "All committed filenames must not match this regex, e.g. (jar|exe)$"
}

variable "slack_merge_events" {
  default     = false
  type        = bool
  description = "merge request to slack"
}

variable "slack_merge_request_channel" {
  default     = ""
  type        = string
  description = "channel to update on MR slack"
}
