module "test_project" {
  source         = "../"
  name           = "test-repo"
  path           = "test-repo"
  visibility     = "private"
  description    = "Terraform"
  parent_id      = "331"
  default_branch = "master"

  # Optional Webhook Configuration
  webhooks = [
    {
      url                   = "https://jsonplaceholder.typicode.com/todos/1"
      merge_requests_events = true
    },
  ]

  # Jira Service Integration
  jira_url      = "https://jira.hpc.cam.ac.uk"
  jira_project  = "RCPTEAM"
  jira_username = var.jira_username
  jira_password = var.jira_password

  # Slack Service Integration
  slack_webhook_url                = var.slack_webhook_url
  slack_issues_events              = true
  slack_issue_channel              = "chatops-testing"
  slack_confidential_issues_events = true
  slack_confidential_issue_channel = "chatops-testing"
}

