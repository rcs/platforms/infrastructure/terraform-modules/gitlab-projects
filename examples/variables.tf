variable "jira_username" {
  default     = ""
  type        = string
  description = "Jira Username to use"
}

variable "jira_password" {
  default     = ""
  type        = string
  description = "Jira password to use"
}

variable "slack_webhook_url" {
  default     = ""
  type        = string
  description = "Slack Webhook URL to use"
}

