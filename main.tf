#
# Project Resources
#

resource "gitlab_project" "project" {
  name                                             = var.name
  path                                             = var.path
  archived                                         = var.archived
  approvals_before_merge                           = var.approvals_before_merge
  container_registry_enabled                       = var.container_registry_enabled
  only_allow_merge_if_pipeline_succeeds            = var.only_allow_merge_if_pipeline_succeeds
  shared_runners_enabled                           = var.shared_runners_enabled
  issues_enabled                                   = var.issues_enabled
  only_allow_merge_if_all_discussions_are_resolved = var.only_allow_merge_if_all_discussions_are_resolved
  snippets_enabled                                 = var.snippets_enabled
  wiki_enabled                                     = var.wiki_enabled
  namespace_id                                     = var.parent_id
  description                                      = var.description
  visibility_level                                 = var.visibility # can be private, internal, public
  default_branch                                   = var.default_branch
  tags                                             = var.tags
}

resource "gitlab_branch_protection" "project_master" {
  count              = length(var.enable_branch_protection)
  project            = gitlab_project.project.id
  branch             = lookup(var.enable_branch_protection[count.index], "branch", "master")
  merge_access_level = lookup(var.enable_branch_protection[count.index], "merge_access_level", "maintainer")
  push_access_level  = lookup(var.enable_branch_protection[count.index], "push_access_level", "developer")
  depends_on         = [gitlab_project.project]
}

resource "gitlab_project_hook" "project_webhook" {
  count                   = length(var.webhooks)
  project                 = gitlab_project.project.id
  enable_ssl_verification = lookup(var.webhooks[count.index], "enable_ssl_verification", true)
  issues_events           = lookup(var.webhooks[count.index], "issues_events", false)
  job_events              = lookup(var.webhooks[count.index], "job_events", false)
  pipeline_events         = lookup(var.webhooks[count.index], "pipeline_events", false)
  tag_push_events         = lookup(var.webhooks[count.index], "tag_push_events", false)
  note_events             = lookup(var.webhooks[count.index], "note_events", false)
  push_events             = lookup(var.webhooks[count.index], "push_events", false)
  url                     = lookup(var.webhooks[count.index], "url", "")
  merge_requests_events   = lookup(var.webhooks[count.index], "merge_requests_events", false)
  wiki_page_events        = lookup(var.webhooks[count.index], "wiki_events", false)
}

resource "gitlab_service_jira" "project_jira" {
  count    = var.jira_url != "" ? 1 : 0
  project  = gitlab_project.project.id
  url      = var.jira_url
  username = var.jira_username # TF_VAR_jira_username
  password = var.jira_password # TF_VAR_jira_password
  # jira_issue_transition_id = var.jira_issue_transition_id
  project_key = var.jira_project
}

resource "gitlab_service_slack" "project_slack" {
  count                        = var.slack_webhook_url != "" ? 1 : 0
  project                      = gitlab_project.project.id
  webhook                      = var.slack_webhook_url
  username                     = var.slack_username
  notify_only_broken_pipelines = var.slack_notify_only_broken_pipelines
  branches_to_be_notified   = var.slack_notify_only_default_branch
  push_events                  = var.slack_push_events
  push_channel                 = var.slack_push_channel
  issues_events                = var.slack_issues_events
  issue_channel                = var.slack_issue_channel
  confidential_issues_events   = var.slack_confidential_issues_events
  confidential_issue_channel   = var.slack_confidential_issue_channel
  tag_push_events              = var.slack_tag_push_events
  tag_push_channel             = var.slack_tag_push_channel
  note_events                  = var.slack_note_events
  note_channel                 = var.slack_note_channel
  pipeline_events              = var.slack_pipeline_events
  pipeline_channel             = var.slack_pipeline_channel
  wiki_page_events             = var.slack_wiki_page_events
  wiki_page_channel            = var.slack_wiki_page_channel
  merge_requests_events        = var.slack_merge_events
  merge_request_channel        = var.slack_merge_request_channel
}

resource "gitlab_tag_protection" "TagProtect" {
  project             = gitlab_project.project.id
  tag                 = var.tag_to_protect
  create_access_level = var.tag_create_access_level
}
