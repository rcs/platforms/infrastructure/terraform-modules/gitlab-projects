# Terraform Module for Defining Gitlab Projects

This module conforms to the [terraform standard module
structure](https://www.terraform.io/docs/modules/create.html#standard-module-structure).

Configuration variables are documented in [variables.tf](variables.tf).

## Usage Examples

The [examples](examples/) directory contains examples of use. A basic usage
example is available in [examples/main.tf](examples/main.tf).

## Users

WIP:

Only accounts with top level admin access may manage users. Instead, we pull users from the Gitlab API to find their IDs. In the examples folder there is a [users.tf](examples/users.tf) file which defines users as an external data source. In order to use it you'll need to have exported your personal access token: `export GITLAB_TOKEN=tokenhere`.


## Projects
See [main.tf](examples/main.tf) for an example of using this module to manage projects. Many options are available: [variables.tf](variables.tf)

## Groups
There is another module for managing Gitlab Groups:
[Gitlab Groups](https://gitlab.developers.cam.ac.uk/rcs/platforms/infrastructure/terraform-modules/gitlab-groups)